/* Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <pango/pango.h>
#include <libguile.h>

static void
pango_attr_list__finalize (gpointer attrs)
{
  pango_attr_list_unref ((PangoAttrList *) attrs);
}

static SCM
scm_baka_splice_attributes (SCM scm_attrs, SCM scm_others, SCM pos, SCM len)
{
  PangoAttrList *attrs, *others;
  scm_to_pointer (scm_attrs);
  if (attrs)
    attrs = pango_attr_list_copy (attrs);
  else
    attrs = pango_attr_list_new ();
  scm_remember_upto_here_1 (scm_attrs);
  others = scm_to_pointer (scm_others);
  pango_attr_list_splice (attrs, others, scm_to_int (pos), scm_to_int (len));
  scm_remember_upto_here_1 (scm_others);
  return scm_from_pointer (attrs, pango_attr_list__finalize);
}
static SCM
scm_baka_parse_markup (SCM markup)
{
  gchar *text = scm_to_utf8_string (markup), *cleaned_text = NULL;
  PangoAttrList *attrs = NULL;
  GError *error = NULL;
  SCM ret = SCM_UNDEFINED;

  if (pango_parse_markup (text, -1, 0, &attrs, &cleaned_text, NULL, &error))
    {
      SCM ret_values[2];
      ret_values[0] = scm_from_utf8_string (cleaned_text);
      g_free (cleaned_text);
      ret_values[1] = scm_from_pointer (attrs, pango_attr_list__finalize);
      ret = scm_c_values (ret_values, 2);
    }
  else
    {
      SCM error_msg = scm_from_utf8_string (error->message);
      g_clear_error (&error);
      scm_misc_error ("parse-markup", "~A", scm_list_1 (error_msg));
    }
  return ret;
}

void
baka_init_scm_markup ()
{
  scm_c_define_gsubr ("%splice-attributes", 4, 0, 0, scm_baka_splice_attributes);
  scm_c_define_gsubr ("parse-markup", 1, 0, 0, scm_baka_parse_markup);
}
