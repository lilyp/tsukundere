;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (baka utils)
  #:use-module (ice-9 iconv)
  #:use-module (sdl2)
  #:export (sdl-color->rgba! string-nbytes))

(define (sdl-color->rgba! rgba color)
  (f64vector-set! rgba 0 (/ (color-r color) 255))
  (f64vector-set! rgba 1 (/ (color-g color) 255))
  (f64vector-set! rgba 2 (/ (color-b color) 255))
  (f64vector-set! rgba 3 (/ (color-a color) 255)))

(define (string-nbytes string)
  (array-length (string->bytevector string "UTF-8")))
