;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere entities debug)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (sdl2)
  #:use-module (tsukundere agenda)
  #:use-module (tsukundere components)
  #:use-module (tsukundere components text)
  #:use-module (tsukundere entities)
  #:use-module (tsukundere entities plain)
  #:use-module ((tsukundere game internals) #:select (*game-agenda*))
  #:export (make-fps-counter activate-fps-counter! deactivate-fps-counter!))

(define *debug-registry* (make-registry))

(define FPS_N_MEASURES 16)
(define-component past *debug-registry* 'past)
(define-component updater *debug-registry* 'updater)

(define (update-fps-counter! counter dt)
  (let ((cur (* 1000 (/ 1 dt))))
    (set! (past counter)
          (cons cur (take (past counter) (1- FPS_N_MEASURES))))
    (reset-text! counter
                 (format #f "~,1f"
                         (/ (* (apply + (past counter)))
                            FPS_N_MEASURES))
                 #t)))

;; Activate @var{counter}, making it visible and starting the count.
(define (activate-fps-counter! counter)
  (schedule-plan! *game-agenda* (updater counter))
  (set! (past counter) (make-list FPS_N_MEASURES 0))
  (set! (visible? counter) #t))

;; Deactivate @var{counter}, hiding it and stopping the count.
(define (deactivate-fps-counter! counter)
  (set! (visible? counter) #f)
  (drop-plan! *game-agenda* (updater counter)))

;; Construct an FPS counter.  If it is @var{active?}, activate it immediately.
;; @var{name}, @var{position} and @var{anchor} are as for generic entities.
;; @var{dimensions}, @var{font}, @var{fill-color}, @var{stroke-color},
;; @var{stroke-width} and @var{alignment} are as for text fields.
(define* (make-fps-counter #:optional (dimensions #s32(256 64))
                           #:key
                           (active? #f)
                           (name 'fps-counter)
                           (font %default-font)
                           (position #s32(0 0))
                           (fill-color (make-color 255 255 255 255))
                           (stroke-color (make-color 0 0 0 255))
                           (stroke-width 1.0)
                           (anchor 'top-left)
                           (alignment '(left . top)))
  (let ((e (make-text dimensions
                      #:name name
                      #:font font
                      #:position position
                      #:fill-color fill-color
                      #:stroke-color stroke-color
                      #:stroke-width stroke-width
                      #:anchor anchor
                      #:alignment alignment)))
    (set! (updater e) (cute update-fps-counter! e <>))
    (if active?
        (activate-fps-counter! e)
        (set! (visible? e) #f))
    e))
