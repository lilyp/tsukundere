;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere entities person)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-26)
  #:use-module (tsukundere assets)
  #:use-module (tsukundere components)
  #:use-module (tsukundere components textures)
  #:use-module (tsukundere entities)
  #:use-module (tsukundere entities plain)
  #:use-module ((tsukundere script utils) #:select (say))
  #:export (load-person person->procedure refresh-texture! attribute))

(define *person-registry* (make-registry))
(define *attribute-registry* (make-registry))
(define-component display-name *person-registry* 'name)
(define-component person-sprites *person-registry* 'sprites)
(define-component attributes *person-registry* 'attributes)

;; Return an accessor for the attribute named @var{attr}.
(define (attribute attr)
  (component->procedure *attribute-registry* attr))

;; Refresh @var{person}'s texture based on their attributes.
;; It is not necessary to call this procedure when using
;; @code{person->procedure}
(define (refresh-texture! person)
  (let ((attributes (attributes person))
        (sprites (person-sprites person)))
    (let ((key (false-if-exception
                (string->symbol
                 (string-join
                  (map (compose
                        symbol->string
                        (cute entity-get *attribute-registry* person <>))
                       attributes)
                  "/")))))
      (unless key
        (error "~a: some attributes are unset" person))
      (set! (texture person)
        (and key (hashq-ref (person-sprites person) key)))
      (unless (texture person)
        (error "~a: cannot find sprite ~a" person key)))))

(define (person-set! person key value)
  (cond
   ((memq key (attributes person))
    (entity-set! *attribute-registry* person key value))
   ((memq key '(position angle anchor))
    (entity-set! *registry* person key value))))

;; Return a procedure that can be called
;; @itemize
;; @item without arguments to return the underlying entity,
;; @item with a string and optional arguments, to use the person as a speaker,
;; @item with @code{#:attribute value ...} to set the person's attributes.
;; @end itemize
;; Use this in preference to manual invocations of @code{attribute} and
;; @code{refresh-texture!}.
(define (person->procedure person)
  (match-lambda*
    (()
     (unless (texture person)
       (error "~a has not yet been initialized" person))
     person)
    (((? keyword? field) value (? keyword? next-field) . rest)
     (person-set! person (keyword->symbol field) value)
     (apply (person->procedure person) next-field rest))
    (((? keyword? field) value . rest)
     (person-set! person (keyword->symbol field) value)
     (refresh-texture! person)
     (apply (person->procedure person) rest))
    (((? string? text) . args)
     (apply say (display-name person) text args))))

(define (initialize-attributes! person attributes)
  (entity-set!
   *person-registry*
   person 'attributes
   (map
    (match-lambda
      (((? symbol? attr) . (? symbol? value))
       (registry-add-component! *attribute-registry* attr)
       (entity-set! *attribute-registry* person attr value)
       attr)
      ((? symbol? attr)
       (registry-add-component! *attribute-registry* attr)
       attr))
    attributes)))

(define (%load-person person dir pattern)
  (set! (person-sprites person)
    (alist->hashq-table (load-images dir pattern (attributes person))))
  person)

;; Load a person from @var{dir} using @var{pattern} and @var{attributes}.
;; @var{pattern} has the same meaning as in @code{load-images}.
;; @var{attributes} is a list of symbols or (@var{name} . @var{value}) pairs
;; in which both @var{name} and @var{value} are themselves symbols.  The plain
;; symbols and names are used to construct the @var{keys} argument of
;; @code{load-images}.  The optional values are used to initialize the
;; attributes of the returned person.
(define* (load-person dir pattern attributes
                      #:key display-name
                      (position #s32(0 0))
                      (angle 0)
                      (anchor 'center))
  (let ((person (make-entity)))
    (entity-set! *registry* person 'position position)
    (entity-set! *registry* person 'angle angle)
    (entity-set! *registry* person 'anchor anchor)
    (entity-set! *registry* person 'render render-texture-entity)
    (entity-set! *person-registry* person 'name display-name)
    (initialize-attributes! person attributes)
    (%load-person person dir pattern)))
