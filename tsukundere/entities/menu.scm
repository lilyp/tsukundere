;; Copyright © 2021 Nicolò Balzarotti <nicolo@nixo.xyz>
;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere entities menu)
  #:use-module ((baka text) #:prefix baka:)
  #:use-module (baka utils)
  #:use-module (ice-9 match)
  #:use-module (sdl2)
  #:use-module (sdl2 rect)
  #:use-module (sdl2 surface)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (tsukundere components)
  #:use-module (tsukundere components menu)
  #:use-module (tsukundere components text)
  #:use-module (tsukundere components textures)
  #:use-module (tsukundere entities)
  #:use-module (tsukundere entities plain)
  #:use-module (tsukundere math)
  #:use-module (tsukundere game)
  #:use-module (tsukundere game internals)
  #:use-module (tsukundere i18n)
  #:use-module (tsukundere script)
  #:use-module (tsukundere script events)
  #:export (make-menu-layer
            run-menu
            button-selected? button-pressed?
            button-select! button-press! button-unselect! button-unpress!
            simple-button-callback radio-button-callback checkbox-button-callback))

(define-component text-offset *menu-registry* 'offset)
(define-component text-font *menu-registry* 'font)
(define-component text-alignment *menu-registry* 'text-alignment)
(define-component text-stroke-width *menu-registry* 'text-stroke-width)
(define-component text-spacing *menu-registry* 'text-spacing)
(define-component text-field-texture *menu-registry* 'text-field-texture)
(define-component button-spacing *menu-registry* 'spacing)

(define button (component->procedure *registry* 'button))
(define menu (component->procedure *registry* 'menu))
(define texture (component->procedure *registry* 'texture))

(define (render-button button)
  (render-texture-entity button)
  (let ((r (button-rect button)))
    (%render-texture (text-field-texture button)
                     (array+ (s32vector (rect-x r) (rect-y r))
                             (text-offset button))
                     anchor:top-left
                     (angle button))))

(define (options->buttons menu options)
  (reverse
   (let ((%button (button menu))
         (%anchor (anchor menu))
         (spacing (button-spacing menu)))
     (let next-button ((pos (position menu))
                       (idx 0)
                       (options options)
                       (buttons '()))
       (if (null? options) buttons
           (let* ((label value state (match (car options)
                                       ((label)
                                        (values (G_ label) label '()))
                                       ((label value)
                                        (values (G_ label) value '()))
                                       ((label value state)
                                        (values (G_ label) value state))
                                       (label (values (G_ label) idx '())))))

             (next-button
              (array+ pos spacing)
              (1+ idx)
              (cdr options)
              (cons
               (let ((btn (make-entity)))
                 (set! (button btn) %button)
                 (set! (anchor btn) %anchor)
                 (set! (position btn) pos)
                 (set! (angle btn) (angle menu))
                 (set! (button-rect btn) (%button-rect %button pos %anchor))
                 (set! (button-state btn) state)
                 (set! (button-value btn) value)
                 (set! (text-field btn)
                       (baka:make-text-field
                        #:fill-rgba
                        (or (hash-ref (button-text-fill-colors %button)
                                      (simplified-button-state btn))
                            (hash-ref (button-text-fill-colors %button) 'none))
                        #:stroke-rgba
                        (or (hash-ref (button-text-stroke-colors %button)
                                      (simplified-button-state btn))
                            (hash-ref (button-text-stroke-colors %button) 'none))
                        #:stroke-width (text-stroke-width menu)))
                 (set! (text-field-texture btn)
                       (make-streaming-texture
                        baka:%text-pixel-format
                        (rect-width (button-rect btn))
                        (rect-height (button-rect btn))))

                 (set! (baka:text-content (text-field btn)) label)
                 (set! (baka:text-font (text-field btn)) (text-font menu))
                 (set! (baka:text-spacing (text-field btn))
                       (text-spacing menu))
                 (set! (text-length btn) (string-length label))
                 (set! (baka:text-num-revealed (text-field btn)) (text-length btn))

                 (set! (baka:text-alignment (text-field btn))
                       (text-alignment menu))

                 (baka:paint! (text-field-texture btn) (text-field btn))

                 (set! (texture btn)
                   (or (hash-ref (button-textures %button)
                                 (simplified-button-state btn))
                       (hash-ref (button-textures %button) 'none)))
                 (set! (text-offset btn) (text-offset menu))
                 (entity-set! *registry* btn 'render render-button)
                 btn)
               buttons))))))))

(define (button-update! btn state)
  (set! (button-state btn) state)
  (let ((state (simplified-button-state btn))
        (text (text-field btn))
        (textures (button-textures (button btn)))
        (text-fill-colors (button-text-fill-colors (button btn)))
        (text-stroke-colors (button-text-stroke-colors (button btn))))
    (sdl-color->rgba! (baka:text-fill-rgba text)
                      (or (hash-ref text-fill-colors state)
                          (hash-ref text-fill-colors 'none)))

    (sdl-color->rgba! (baka:text-stroke-rgba text)
                      (or (hash-ref text-stroke-colors state)
                          (hash-ref text-stroke-colors 'none)))
    (baka:paint! (text-field-texture btn) text)

    (set! (texture btn) (or (hash-ref textures state)
                            (hash-ref textures 'none)))))

(define-inlinable (button-selected? btn)
  (member 'selected (button-state btn)))

(define-inlinable (button-pressed? btn)
  (member 'selected (button-state btn)))

(define-inlinable (button-select! btn)
  (button-update! btn (lset-adjoin eq? (button-state btn) 'selected)))

(define-inlinable (button-press! btn)
  (button-update! btn (lset-adjoin eq? (button-state btn) 'pressed)))

(define-inlinable (button-unselect! btn)
  (button-update! btn (delete 'selected (button-state btn))))

(define-inlinable (button-unpress! btn)
  (button-update! btn (delete 'pressed (button-state btn))))

(define (default-accumulator buttons)
  (fold
   (lambda (btn val)
     (if (button-pressed? btn)
         (button-value btn)
         val))
   *unspecified*
   buttons))

(define default-validator (negate unspecified?))

;; Button callback for simple buttons.  Can be pressed with either the
;; keyboard return or space key, or with a left click of a mouse.
(define (simple-button-callback menu button . action)
  "A button callback which implements simple buttons."
  (match action
    (('key-press (? (cute member <> '(return space)) key))
     (button-press! button)
     #t)
    (('mouse-press 'left _count _pos)
     (button-press! button)
     #t)
    (_ #f)))

;; Button callback for radio buttons.  Can be pressed with either the
;; keyboard space key, or with a left click of a mouse.  When one is
;; selected, others are deselected.  Break the loop on return.
(define (radio-button-callback menu button . action)
  "A button callback which implements radio buttons."
  (define (radio-clicked button)
    (unless (button-pressed? button)
      (map button-unpress! (layer menu))
      (button-press! button))
    #f)
  (match action
    (('key-press 'return) #t)
    (('key-press 'space) (radio-clicked button))
    (('mouse-press 'left _count _pos) (radio-clicked button))
    (_ #f)))

;; Button callback for checkbox buttons.  Can be pressed with either
;; the keyboard space key, or with a left click of a mouse.  Break the
;; loop on return.
(define (checkbox-button-callback menu button . action)
  "A button callback which implements multiple checkbox."
  (define (checkbox-clicked button)
    (if (button-pressed? button)
        (button-unpress! button)
        (button-press! button))
    #f)
  (match action
    (('key-press 'return) #t)
    (('key-press 'space) (checkbox-clicked button))
    (('mouse-press 'left _count _pos) (checkbox-clicked button))
    (_ #f)))

;; Construct a new menu layer from @var{base} using @var{options}.
;; Use @var{position}, @var{spacing} and @var{anchor} to lay out @var{options}.
;; Use @var{button} for the lookup of text-colors and textures.
;; Use @var{accumulator}, @var{validator} and @var{button-callback} for the
;; menu's logic.
;; Use @var{text-offset}, @var{text-font} and @var{text-alignment} to lay out
;; text inside the button.
;; Infer missing keyword arguments from @var{base} if given.
;; Alternatively, if @var{buttons} is given, use them directly instead of
;; constructing new ones.
(define* (make-menu-layer #:optional options
                          #:key base buttons
                          position angle spacing anchor
                          button accumulator validator button-callback
                          text-offset text-font text-alignment
                          text-spacing text-stroke-width)
  (and buttons
       (or options spacing button)
       (raise
        (condition
         (&message
          (message "Cannot make-menu-layer from both a list of buttons and
button customization options")))
        #:continuable? #t))
  (let* ((component (cute component->procedure *registry* <>))
         (menu-component (cute component->procedure *menu-registry* <>))
         (text-component (cute component->procedure *text-registry* <>))
         (@menu (cute compose <> menu))
         (button (or button (and=> base (component 'button))))
         (text-font (or text-font (and=> base (menu-component 'font))
                        %default-font))
         (text-stroke-width (or text-stroke-width
                                (and=> base (menu-component 'text-stroke-width))
                                1.0))
         (text-alignment (or text-alignment
                             (and=> base (menu-component 'text-alignment))
                             '(center . center)))
         (text-spacing (or text-spacing
                           (and=> base (menu-component 'text-spacing))
                           (list 0.5 12)))
         (position (or position (and=> base (component 'position))))
         (anchor (or anchor (and=> base (component 'anchor)) 'center))
         (angle (or angle (and=> base (component 'angle)) 0))
         (spacing (or spacing (and=> base button-spacing)))
         (text-offset (or text-offset (and=> base (menu-component 'offset))
                          #s32(0 0)))
         (accumulator (or accumulator (and=> base (@menu menu-accumulator))
                          default-accumulator))
         (validator (or validator (and=> base (@menu menu-validator))
                        default-validator))
         (button-callback (or button-callback
                              (and=> base (@menu menu-button-callback))
                              simple-button-callback))
         (%layer (make-layer (or (and=> base entity-name) 'menu))))
    (set! (layer %layer) buttons)
    (set! (button-spacing %layer) spacing)
    (set! (menu %layer) (make-menu accumulator validator button-callback))
    (entity-set! *registry* %layer 'position position)
    (entity-set! *registry* %layer 'angle angle)
    (entity-set! *registry* %layer 'anchor anchor)
    (entity-set! *registry* %layer 'button button)
    (entity-set! *menu-registry* %layer 'offset text-offset)
    (entity-set! *menu-registry* %layer 'font text-font)
    (entity-set! *menu-registry* %layer 'text-spacing text-spacing)
    (entity-set! *menu-registry* %layer 'text-stroke-width text-stroke-width)
    (entity-set! *menu-registry* %layer 'text-alignment text-alignment)
    (set! (layer %layer)
      (cond
       (buttons buttons)
       (options (options->buttons %layer options))
       (else '())))
    %layer))

;; Select the button at @var{pos} and unselect all others.
;; It is crucial, that @var{buttons} do not intersect with each other,
;; as otherwise more than one button may be selected.
(define (menu-select-button-at-pos! buttons pos)
  (for-each
   (lambda (btn)
     (if (inside-rect? pos (button-rect btn) (angle btn))
         (button-select! btn)
         (button-unselect! btn)))
   buttons))

(define (menu-every menu-layer . args)
  (fold
   (lambda (btn val)
     (and val
          (or (not (button-selected? btn))
              (apply (menu-button-callback (menu menu-layer))
                     menu-layer btn args))))
   #t (layer menu-layer)))

(define (menu-any menu-layer . args)
  (fold
   (lambda (btn val)
     (or val
         (and (button-selected? btn)
              (apply (menu-button-callback (menu menu-layer))
                     menu-layer btn args))))
   #f (layer menu-layer)))

;; Pause the current scene and spawn a new one, displaying @var{menu-layer}.
;; @var{menu-layer} should have been created using @code{make-menu-layer}.
;; Run until the @var{menu-validator} of its @var{menu} is satisfied.
;; Return the result of applying its @var{menu-accumulator} to the buttons
;; in @var{menu-layer}.
(define* (run-menu menu-layer)
  (define (prev idx list)
    (modulo/length (1- (or idx 0)) list))
  (define (next idx list)
    (modulo/length (1+ (or idx -1)) list))

  (define (arrow-key buttons direction)
    (let ((idx (list-index button-selected? buttons)))
      ;; deselect current button & select next one
      (when idx (button-unselect! (list-ref buttons idx)))
      (button-select! (list-ref buttons (direction idx buttons)))))

  (define (shift-pressed? mode)
    (not (null? (lset-intersection eq? '(left-shift right-shift) mode))))

  (let* ((buttons (layer menu-layer))
         (menu (menu menu-layer))
         (running? #t)
         (previous-handler (current-event-handler)))
    (when (null? buttons)
      (error "Empty options in menu not allowed!"))

    (call/paused
     (lambda ()
       (while running?

         (set-script-skipping! (current-script) #f)
         (sleep 1)))
     #:scene
     (let* ((old-scene (current-scene))
            (layers (map entity-name (layer old-scene)))
            (entities (map layer (layer old-scene)))
            (scene (make-scene layers)))
       (for-each %layer-append! (layer scene) entities)
       (layer-replace! scene (entity-name menu-layer) menu-layer)
       (unless (memq menu-layer (layer scene))
         (error "Menu is not a part of the scene!"))
       ;; Copy visibilities, then hide menu if text is hidden
       (for-each (setter visible?) (layer scene) (map visible? (layer old-scene)))
       (match (entity-at old-scene 'text)
         (#f *unspecified*)
         (text
          (unless (visible? text)
            (set! (visible? menu-layer) #f))))

       scene)
     #:key-press
     (lambda (key code mode repeat)
       (match `(,key ,mode)
         (('h _)
          (unless repeat
            (set! (visible? menu-layer) (not (visible? menu-layer))))
          ((on-key-press previous-handler) key code mode repeat))
         (('up _) (arrow-key buttons prev))
         (('down _) (arrow-key buttons next))
         (('tab (? shift-pressed?)) (arrow-key buttons prev))
         (('tab _) (arrow-key buttons next))
         (('return _)
          ;; If nothing is selected, return just check for
          ;; menu-validator to be #t before breaking the loop, meaning
          ;; that, unlike for 'space, each menu which is valid will
          ;; break upon 'return.
          (when (and (menu-every menu-layer 'key-press key)
                     ((menu-validator menu) ((menu-accumulator menu) buttons)))
            (set! running? #f)))
         (('space _)
          ;; Differnently from 'return, this won't break unless a
          ;; button is selected.  This allows some buttons to break on
          ;; 'space (like simple-button) but not on others (breking
          ;; checkboxes with space would be strange, as space is also
          ;; used for toggling and the behaviour would change only
          ;; based on button-selected?.
          (when (and (menu-any menu-layer 'key-press key)
                     ((menu-validator menu) ((menu-accumulator menu) buttons)))
            (set! running? #f)))
         (_
          ((on-key-press previous-handler) key code mode repeat))))
     #:key-release (on-key-release previous-handler)
     #:mouse-press
     (lambda (btn count pos)
       (menu-select-button-at-pos! buttons pos)
       (when (menu-any menu-layer 'mouse-press btn count pos)
         (set! running? #f)))
     #:mouse-release
     (lambda (btn pos)
       (menu-select-button-at-pos! buttons pos)
       (when (menu-any menu-layer 'mouse-release btn pos)
         (set! running? #f)))
     #:mouse-move
     (lambda (_ pos rel)
       (menu-select-button-at-pos! buttons pos)
       (when (menu-any menu-layer 'mouse-move pos)
         (set! running? #f)))
     #:text-input (const *unspecified*)
     #:hard-pause? #t)
    ((menu-accumulator menu) buttons)))
