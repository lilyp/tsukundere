;; Copyright © 2020 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere utils)
  #:use-module (ice-9 match)
  #:export (xdg-data-dir
            tsukundere-config-dir
            tsukundere-config-file
            tsukundere-data-dir
            tsukundere-data-file
            mkdir-p
            module->keyword-list))

(define (xdg-config-dir)
  "Return the canonical value of XDG_CONFIG_HOME."
  (string-append
   (or (getenv "XDG_CONFIG_HOME")
       (string-append (getenv "HOME") "/.config"))))

(define (xdg-data-dir)
  "Return the canonical value of XDG_DATA_HOME."
  (string-append
   (or (getenv "XDG_DATA_HOME")
       (string-append (getenv "HOME") "/.local/share"))))

(define (tsukundere-config-dir)
  "Return the directory, in which tsukundere can store configuration."
  (string-append (xdg-config-dir) "/tsukundere"))

(define (tsukundere-config-file f)
  (string-append (tsukundere-config-dir) "/" f))

(define (tsukundere-data-dir)
  "Return the directory, in which tsukundere can store data."
  (string-append (xdg-data-dir) "/tsukundere"))

(define (tsukundere-data-file f)
  (string-append (tsukundere-data-dir) "/" f))

(define (string-empty? s)
  (= (string-length s) 0))

(define (mkdir-p dir)
  "Make DIR including its parents."
  (define (mkdir? path)
    (catch 'system-error
      (lambda ()
        (mkdir path)
        #t)
      (lambda args
        (if (= EEXIST (system-error-errno args))
            #t
            (apply throw args)))))

  (let loop ((components (string-split dir #\/))
             (root (if (string-prefix? "/" dir) "" ".")))
    (cond
     ((null? components) #t)
     ((string-empty? (car components))
      (loop (cdr components) root))
     (else
      (let ((path (string-append root "/" (car components))))
       (and (mkdir? path)
            (loop (cdr components) path)))))))

(define* (module->keyword-list module #:optional (init '()))
  (hash-fold
   (lambda (key value seed)
     (cons* (symbol->keyword key) (variable-ref value) seed))
   init
   (module-obarray module)))
