;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere game support)
  #:use-module (ice-9 arrays)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (system base compile)
  #:use-module (system vm loader)
  #:export (support-language!))

(define *support-table*
  (map array-copy
       '(#(elisp "tsukundere/support/base.el" #f))))

(define support-language (cute vector-ref <> 0))
(define support-file (cute vector-ref <> 1))
(define already-loaded?
  (make-procedure-with-setter
   (cute vector-ref <> 2)
   (cute vector-set! <> 2 <>)))

(define (%support-language entry)
  (unless (already-loaded? entry)
    (compile-and-load (%search-load-path (support-file entry))
                      #:from (support-language entry))
    (set! (already-loaded? entry) #t)))

(define (support-language! language)
  (and=>
   (find (compose (cute eq? <> language) support-language) *support-table*)
   %support-language))
