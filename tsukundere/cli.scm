;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere cli)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-37)
  #:use-module ((tsukundere i18n) #:select ((T_ . G_))) ; rename for format
  #:re-export (option)
  #:export (base-options
            prepend reset
            execute fail show-help show-version))

(define %version "0.4.3")

(define (fail message . args)
  (format (current-error-port) (G_ "error: ~?~%") (G_ message) args)
  (exit 1))

(define (prepend key)
  (lambda (opt name value result)
    (acons key
           (cons value (or (assoc-ref result key) '()))
           result)))

(define* (reset key #:optional (filter identity))
  (lambda (opt name value result)
    (acons key (filter value) result)))

(define base-options
  (list (option '(#\h "help") #f #f
                (lambda (opt name arg result)
                  (acons 'help? #t result)))
        (option '("version") #f #f
                (lambda (opt name arg result)
                  (show-version)
                  (exit 0)))))

(define (show-version)
  (format #t (G_ "Tsukundere ~A~%
Copyright © 2021 Liliana Prikler
License LGPLv3+: GNU LGPL version 3 or later <http://gnu.org/licenses/lgpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.~%") %version))

(define (lookup-command command)
  (let* ((spec (list 'tsukundere 'cli (string->symbol command)))
         (module (false-if-exception (resolve-interface spec))))
    (unless module (fail "no such command: ~A" command))
    module))

(define (*help*)
  (G_ "Usage: tsukundere [COMMAND] [OPTIONS]

Available commands are:
  help              Show this help or the help for a command.
  run               Run a visual novel.
  check             Check a visual novel.
  extract-dialogue  Extract dialogue and other translatable strings.
  version           Show the version of Tsukundere."))

(define (show-help command)
  (if command
      (display ((module-ref (lookup-command command) '*help*)))
      (display (*help*)))
  (newline)
  (exit 0))

(define (execute command args)
  (let ((cmd (string->symbol command))
        (module (lookup-command command)))
    (apply (module-ref module cmd)
           (fold
            (lambda (pair seed)
              (cons* (symbol->keyword (car pair)) (cdr pair) seed))
            '()
            (args-fold
             args (module-ref module '*options*)
             (lambda (opt name arg result)
               (format (current-error-port)
                       (G_ "~A: unrecognized option~%") name)
               (exit 1))
             (lambda (file result)
               (let ((%files (assoc-ref result 'files)))
                 (if %files
                     (acons 'files (cons file %files) result)
                     (fail "not an option: ~A" file))))
             (module-ref module '*values*))))))
