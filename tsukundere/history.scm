(define-module (tsukundere history)
  #:use-module (ice-9 format)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-26)
  #:use-module (tsukundere agenda)
  #:use-module (tsukundere game internals)
  #:use-module (tsukundere script)
  #:use-module (tsukundere utils)
  #:replace (load)
  #:export (save add-load-hook! load-from-state history))

(define *history* (make-hash-table))
(define load-hook (make-hook 1))

(define (%save file state)
  (mkdir-p (dirname file))
  (call-with-output-file file
    (lambda (port)
      (display "#| -*- mode: scheme -*- |#" port)
      (newline port)
      (pretty-print state port))))

(define (%save-file-name game n)
  (tsukundere-data-file (format #f "~a.~d" game n)))

;; Save the current game state for @var{game} in its @var{n}th save slot.
(define (save game n)
  (false-if-exception
   (%save
    (%save-file-name game n)
    (cons*
     #:tsukundere-position
     (reverse (map script-line (car *script-stack*)))
     #:tsukundere-history (hash-map->list cons *history*)
     ((fluid-ref *save-state*))))))

;; Add @var{proc} as a load hook.
;; Load hooks are called with a number of @code{#:keyword value} pairs read
;; from the save state and should allow other keys than the ones they're
;; interested in.
(define* (add-load-hook! proc #:optional (append? #t))
  (add-hook! load-hook (lambda (args) (apply proc args)) append?))

(add-load-hook!
 (lambda* (#:key tsukundere-history #:allow-other-keys)
   (set! *history* (alist->hashq-table tsukundere-history))))

(define (%load filename)
  (call-with-input-file filename read))

;; Load @var{game}'s @var{n}th save.
(define (load game n)
  "Load the state saved into the Nth slot for GAME."
  (and-let* ((filename (%save-file-name game n))
             (state (false-if-exception (%load filename))))
    (load-from-state state filename)))

(define* (load-from-state state #:optional filename)
  (run-hook load-hook (cons* #:tsukundere-save-file filename state)))

(define-syntax history
  (make-variable-transformer
   (lambda (x)
     (syntax-case x (set!)
       ((set! this val)
        (identifier? #'this)
        #'(set! *history* (alist->hashq-table val)))
       ((this var exp)
        (and (identifier? #'this) (identifier? #'var))
        #'(let* ((%handle (hashq-get-handle *history* (quote var)))
                 (val (if %handle (cdr %handle) exp)))
            (unless %handle
              (hashq-set! *history* (quote var) val))
            val))
       ((this var)
        (and (identifier? #'this) (identifier? #'var))
        #'(hashq-ref *history* (quote var)))
       (this (identifier? #'this) #'(hash-map->list cons *history*))))))
