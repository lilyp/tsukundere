;; Copyright © 2020 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere script)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 q)
  #:use-module (tsukundere agenda)
  #:use-module (tsukundere game internals)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-71)
  #:export (make-script
            fresh-script start-script
            script-scene script-event-handler
            script-status set-script-status! resume
            script-line script-clock script-time
            script-restore call-with-restore
            script-skipping? set-script-skipping!
            speed-through-clock! forward-line!
            pause/script script forever
            on-next-script-line)
  #:replace (pause sleep))

(define script-prompt (make-prompt-tag 'tsukundere:script))

(define-record-type <script>
  (%make-script thunk scene event-handler line clock status cont back
                skipping?)
  script?
  (thunk          script-thunk)
  (scene          script-scene)
  (event-handler  script-event-handler)
  (line           %script-line)
  (clock          script-clock)
  (status         script-status        set-script-status!)
  (cont           script-resume        set-script-resume!)
  (back           script-restore)
  (skipping?      script-skipping?     set-script-skipping!))

;; Allocate a new script for @var{thunk}.
(define (make-script thunk scene event-handler)
  (%make-script thunk scene event-handler
                (make-agenda) (make-agenda) 'not-started
                (const *unspecified*)
                (make-hook)
                #f))

;; Return the current `line' of @var{script}, i.e. how often
;; @code{forward-line!} was called.
(define (script-line script)
  (agenda-time (%script-line script)))

;; Return the time spent in @var{script} in milliseconds.
(define (script-time script)
  (agenda-time (script-clock script)))

;; Fast-forward through all tasks that are waiting for more time to be spent
;; in @var{script}.  Return @code{#t} if the script clock changed,
;; @code{#f} otherwise.
(define* (speed-through-clock! script #:optional force?)
  (let loop ((clock (script-clock script))
             (did-skip? #f))
    (cond
     ((not (agenda-filled? clock)) did-skip?)
     ((or force? (script-skipping? script))
      (update-agenda! clock 1)
      (loop clock #t))
     (else did-skip?))))

(define *next-script-line* (make-hook))

;; Forward @var{script} by one `line'.
(define (forward-line! script)
  (run-hook *next-script-line*)
  (reset-hook! *next-script-line*)
  (update-agenda! (%script-line script) 1))

;; Return a copy of @var{script}, that is not yet started.
(define (fresh-script script)
  (if (eq? (script-status script) 'not-started)
      script
      (make-script (script-thunk script) (script-scene script)
                   (script-event-handler script))))

(define* (start-script script)
  (unless (eq? (script-status script) 'not-started)
    (error "attempted to start already started script ~s" script))
  (let ((script (fresh-script script))
        (thunk (script-thunk script)))
    (define (handler cont callback)
      (set-script-resume!
       script
       (lambda ()
         (unless (eq? (script-status script) 'cancelled)
           (call-with-prompt script-prompt cont handler))))
      (callback script))
    (set-script-status! script 'running)
    (call-with-prompt script-prompt
      (lambda ()
        (thunk)
        (set-script-status! script 'finished)
        (unless (eq? script (q-pop! *script-stack*))
          (error "script stack corrupted"))
        (if (q-empty? *script-stack*)
            (abort-to-prompt quit-prompt)
            (forward-line! (caar *script-stack*))))
      handler)
    script))

;; Resume @var{script}.
(define (resume script)
  ((script-resume script)))

(define (%default-pause-handler script)
  (schedule-after! (%script-line script) 1 (script-resume script)))

;; Pause the current script until @var{handler} resumes it.
;; By default handler waits for the player to forward it using @code{step!}.
(define* (pause #:optional (handler %default-pause-handler))
  "Pause the current script until HANDLER calls it again.
By default, that is until step! or skip! is called."
  (abort-to-prompt script-prompt handler))

;; Pause the current script for @var{duration} milliseconds.
(define (sleep duration)
  "Wait DURATION before resuming the current script."
  (pause (lambda (script)
           (schedule-after! (script-clock script) duration
                            (script-resume script)))))

;; Pause the current script for the duration of @var{script}.
;; If @var{hard?} is truthy, stop skipping both when entering and exiting
;; @var{script}, otherwise arrange for fluid transitions.
(define* (pause/script script #:optional hard?)
  (pause
   (lambda (orig-script)
     (let ((%skipping (script-skipping? orig-script)))
       (set-script-skipping! orig-script #f)
       (set-script-skipping! script (if hard? #f %skipping)))
     (q-push! *script-stack* script)

     (schedule-after! (%script-line orig-script) 1
                      (lambda ()
                        (if (eq? (script-status script) 'finished)
                            (unless hard?
                              (set-script-skipping! orig-script
                                                    (script-skipping? script)))
                            (begin
                              (unless (eq? script (q-pop! *script-stack*))
                                (error "script stack corrupted"))
                              (set-script-status! script 'cancelled)))))
     (schedule-after! (%script-line orig-script) 1 (script-resume orig-script))
     (start-script script))))

(define (call-with-restore thunk restore)
  (let ((script (current-script)))
    (add-hook! (script-restore script) restore)
    (let ((result (values->list (thunk))))
      (remove-hook! (script-restore script) restore)
      (unlist result))))

(define-syntax step
  (syntax-rules ()
    ((step exp)
     (begin (pause) exp))
    ((step exp0 exp1 ...)
     (begin exp0 (step exp1) ...))))

(define-syntax-rule (script exp0 exp1 ...)
  (begin
    (step exp0 exp1 ...)))

(define-syntax-rule (forever exp0 exp1 ...)
  (let* ((agenda (%script-line (current-script)))
         (current-line (agenda-time agenda)))
    (while #t
      (reset-agenda! agenda)
      (update-agenda! agenda current-line)
      (begin exp0 exp1 ...))))

(define-syntax-rule (on-next-script-line exp0 exp1 ...)
  (add-hook! *next-script-line*
             (lambda ()
               (begin exp0 exp1 ...))))
