;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere script events)
  #:use-module (tsukundere game internals)
  #:use-module (tsukundere math)
  #:use-module (sdl2)
  #:use-module (sdl2 rect)
  #:use-module (sdl2 events)
  #:use-module (sdl2 render)
  #:use-module (sdl2 video)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-71)
  #:export (event-handler
            handle-events
            fit-current-window! ;; for initialization in (tsukundere game)

            on-key-press
            on-key-release
            on-mouse-press
            on-mouse-release
            on-mouse-move
            on-text-input))

(define-record-type <event-handler>
  (%event-handler key-press key-release mouse-press mouse-release
                  mouse-move text-input)
  event-handler?
  (key-press     on-key-press)
  (key-release   on-key-release)
  (mouse-press   on-mouse-press)
  (mouse-release on-mouse-release)
  (mouse-move    on-mouse-move)
  (text-input    on-text-input))

(define 1/current-window-transformation
  (let ((a0 (make-array 0 2 3)))
    (array-set! a0 1 0 0)
    (array-set! a0 1 1 1)
    a0))

;; Construct an event handler record for the use @code{handle-events}, which
;; see.
;; @*
;; @var{key-press} is called with four arguments when a key is pressed.
;; @itemize
;; @item @var{key}: The key, that was pressed.
;; @item @var{scancode}: The raw scancode of the key, that was pressed.
;; This can be different from @var{key}, but mostly shouldn't.
;; @item @var{modifiers}: Which modifier keys (e.g. shift, control, alt)
;; where also pressed at the same time.
;; @item @var{repeat?}: Whether this key was already pressed down.
;; @end itemize
;; @var{key-release} is likewise called when a key is released with the
;; arguments @var{key}, @var{scancode} and @var{modifiers} as above.
;; @*
;; @var{mouse-press} is called with three arguments when a mouse key is pressed.
;; @itemize
;; @item @var{button}: Which button was pressed.
;; @item @var{clicks}: How often @var{button} has been clicked.
;; @item @var{pos}: The position at which @var{button} was clicked as
;; @code{(s32vector x y)}.
;; @end itemize
;; @var{mouse-release} is likewise called when a mouse button is released
;; with the arguments @var{button}, @var{pos}.
;; @*
;; @var{mouse-move} is called with three arguments when the mouse pointer is
;; moved.
;; @itemize
;; @item @var{buttons}: Which mouse buttons are currently pressed.
;; @item @var{pos}: The position to which the mouse was moved.
;; @item @var{rel}: The relative movement of the mouse.
;; @end itemize
;; @var{text-input} is called with a singular argument @var{text}, whenever
;; a printable key is pressed, where text holds that key(s) as string.
(define* (event-handler #:key
                        key-press
                        key-release
                        mouse-press
                        mouse-release
                        mouse-move
                        text-input)
  (let ((noop (const *unspecified*)))
    (%event-handler (or key-press noop)
                    (or key-release noop)
                    (or mouse-press noop)
                    (or mouse-release noop)
                    (or mouse-move noop)
                    (or text-input noop))))

(define (fit-current-window!)
  (let* ((window (current-window))
         (rw rh (window-minimum-size window))
         (gw gh (window-size window)))
    (if (or (< gw rw) (< gh rh))
        (begin
          (set-window-resizable! window #f)
          (set-window-size! window (min gw rw) (min gh rh))
          (fit-current-window!)
          (set-window-resizable! window #t))
        (let* ((scale (min (/ gw rw) (/ gh rh)))
               (w (floor (* rw scale)))
               (h (floor (* rh scale)))
               (x (floor/ (- gw w) 2))
               (y (floor/ (- gh h) 2)))
          (array-set! 1/current-window-transformation (/ 1 scale) 0 0)
          (array-set! 1/current-window-transformation (/ 1 scale) 1 1)
          (array-set! 1/current-window-transformation (- x) 0 2)
          (array-set! 1/current-window-transformation (- y) 1 2)
          (set-renderer-viewport! (current-renderer)
                                  (make-rect x y w h))
          (set-renderer-scale! (current-renderer) scale scale)))))

;; Pre-process @var{event} and pass it to the correct field of @var{handler}.
(define (handle-event event handler)
  (cond
   ((quit-event? event) (abort-to-prompt quit-prompt))
   ((keyboard-down-event? event)
    ((on-key-press handler)
     (keyboard-event-key event)
     (keyboard-event-scancode event)
     (keyboard-event-modifiers event)
     (keyboard-event-repeat? event)))
   ((keyboard-up-event? event)
    ((on-key-release handler)
     (keyboard-event-key event)
     (keyboard-event-scancode event)
     (keyboard-event-modifiers event)))
   ((mouse-button-down-event? event)
    ((on-mouse-press handler)
     (mouse-button-event-button event)
     (mouse-button-event-clicks event)
     (array->s32vector
      (array*
       1/current-window-transformation
       (let ((v (make-array 1 3 1)))
         (array-set! v (mouse-button-event-x event) 0 0)
         (array-set! v (mouse-button-event-y event) 1 0)
         v)))))
   ((mouse-button-up-event? event)
    ((on-mouse-release handler)
     (mouse-button-event-button event)
     (array->s32vector
      (array*
       1/current-window-transformation
       (let ((v (make-array 1 3 1)))
         (array-set! v (mouse-button-event-x event) 0 0)
         (array-set! v (mouse-button-event-y event) 1 0)
         v)))))
   ((mouse-motion-event? event)
    ((on-mouse-move handler)
     (mouse-motion-event-buttons event)
     (array->s32vector
      (array*
       1/current-window-transformation
       (let ((v (make-array 1 3 1)))
         (array-set! v (mouse-motion-event-x event) 0 0)
         (array-set! v (mouse-motion-event-y event) 1 0)
         v)))
     (array->s32vector
      (array*
       1/current-window-transformation
       (let ((v (make-array 0 3 1)))
         (array-set! v (mouse-motion-event-x-rel event) 0 0)
         (array-set! v (mouse-motion-event-y-rel event) 1 0)
         v)))))
   ((text-input-event? event)
    ((on-text-input handler) (text-input-event-text event)))
   ((window-resized-event? event)
    (fit-current-window!))))

;; Poll SDL events and handle them.
(define* (handle-events #:optional (handler (current-event-handler)))
  (let ((event (poll-event)))
    (when event
      (handle-event event handler)
      (handle-events handler))))
