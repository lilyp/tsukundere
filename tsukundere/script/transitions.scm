;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere script transitions)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (sdl2 render)
  #:use-module (tsukundere math)
  #:use-module (tsukundere script)
  #:use-module (tsukundere script utils)
  #:use-module (tsukundere entities plain)
  #:export (transition->procedure))

;; Retrieve the procedure named @var{transition}, if @var{transition} is a
;; symbol, otherwise return it as-is if it is a procedure.
;; @anchor{transitions}Transition procedures should take four arguments
;; @itemize
;; @item the @var{duration} of the transition,
;; @item the @var{layer-entity} to which items are added,
;; @item @var{new-items} to add to its back, and
;; @item @var{replace?}, which if truthy causes the transition to entirely
;; replace the layer with @var{new-items}.
;; @end itemize
(define (transition->procedure transition)
  (match transition
    ((procedure? proc) proc)
    ('alpha transition:alpha)
    ('black transition:black)
    (#f transition:none)
    (_ (error "transition ~a is not supported" transition))))

;; Add @var{new-items} to @var{layer-entity}, slowly increasing their opacity
;; for @var{duration} milliseconds.  If @var{replace?} is truthy, entirely
;; replace the layer afterwards.
(define (transition:alpha duration layer-entity new-items replace?)
  (set! (layer layer-entity) (append (layer layer-entity) new-items))
  (tween duration 0 255
         (lambda (alpha)
           (for-each
            (lambda (entity)
              (and=> (texture entity) (cute set-texture-alpha-mod! <> alpha)))
            new-items))
         #:interpolate (compose ->integer lerp))
  (when replace? (set! (layer layer-entity) new-items)))

;; Fade all items within @var{layer-entity} to black, then add
;; @var{new-items} (replacing old items if @var{replace?}) is truthy,
;; and finally fade from black.
;; The entire operation should have visible effects for @var{duration}
;; milliseconds and all previous items in @var{layer-entity} should be
;; restored to their previous colour at the end.
(define (transition:black duration layer-entity new-items replace?)
  (let* ((before (if (and replace? (null? new-items))
                     duration
                     (floor-quotient duration 2)))
         (after (- duration before))
         (old-items (layer layer-entity))
         (n (length old-items))
         (all-items (append old-items new-items))
         (all-textures (map texture all-items))
         (old-textures (take all-textures n))
         (all-textures (filter identity all-textures))
         (old-textures (filter identity old-textures))
         (all-color-mod (map texture-color-mod all-textures))
         (old-color-mod (map texture-color-mod old-textures)))
    (call-with-restore
     (lambda ()
       (tween before 1 0
              (lambda (mod)
                (for-each (lambda (texture m)
                            (apply set-texture-color-mod! texture
                                   (map (compose ->integer (cute * mod <>)) m)))
                          old-textures
                          old-color-mod))
              #:restore? #f)
       (set! (layer layer-entity) (if replace? all-items new-items))
       (tween after 0 1
              (lambda (mod)
                (for-each (lambda (texture m)
                            (apply set-texture-color-mod! texture
                                   (map (compose ->integer (cute * mod <>)) m)))
                          all-textures
                          all-color-mod))
              #:restore? #f))
     (lambda ()
       (for-each (lambda (texture mod)
                   (apply set-texture-color-mod! texture mod))
                 all-textures all-color-mod)))))

;; If @var{replace?} is truthy, replace the layer of @var{layer-entity} with
;; @var{new-items}, otherwise append them.
(define (transition:none duration layer-entity new-items replace?)
  (set! (layer layer-entity)
        (if replace? new-items (append (layer layer-entity) new-items))))
