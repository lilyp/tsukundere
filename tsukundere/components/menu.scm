;; Copyright © 2021 Nicolò Balzarotti <nicolo@nixo.xyz>
;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere components menu)
  #:use-module (ice-9 arrays)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 match)
  #:use-module (tsukundere components)
  #:use-module (tsukundere components textures)
  #:use-module (tsukundere math)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:use-module (sdl2 rect)
  #:export (*menu-registry*
            make-button button?
            button-state simplified-button-state
            button-textures button-text-fill-colors button-text-stroke-colors
            button-value
            button-rect %button-rect
            make-menu menu?
            menu-accumulator
            menu-validator
            menu-button-callback))

(define *menu-registry* (make-registry))

(define-record-type <button>
  (%make-button textures text-fill-colors text-stroke-colors %dimensions)
  button?
  (textures button-textures)
  (text-fill-colors button-text-fill-colors)
  (text-stroke-colors button-text-stroke-colors)
  (%dimensions button-dimensions))
(registry-add-component! *registry* 'button)
(define-component button-state *menu-registry* 'state)
(define-component button-value *menu-registry* 'value)

(define (%button-rect button position anchor)
  (let* ((dim (button-dimensions button))
         (anchor (anchor->procedure anchor))
         (pos (anchor position dim))
         (w (array-ref dim 0))
         (h (array-ref dim 1))
         (x (array-ref pos 0))
         (y (array-ref pos 1)))
    (make-rect x y w h)))
(define-component button-rect *menu-registry* 'button-rect)

(define (max-texture-dimensions textures)
  (let* ((v #s32(0 0))
         (out (array-copy v)))
    (apply array-map! out max v
           (hash-map->list (lambda (_ val) (texture-dimensions val)) textures))
    out))

;; Create a new button with @var{textures} and @var{text-colors}.
;; Both can either be an alist mapping symbols to textures or colors
;; respectively, or a hash-table with the same mapping (using @code{equal?}
;; for comparison).  The keys should be simplified button states, see
;; @code{simplify-button-state}.
(define* (make-button textures text-fill-colors
                      #:optional (text-stroke-colors text-fill-colors)
                      #:key dimensions)
  (let ((textures-hash-table (if (hash-table? textures) textures
                                 (alist->hash-table textures)))
        (text-fill-colors-hash-table
         (if (hash-table? text-fill-colors)
             text-fill-colors
             (alist->hash-table text-fill-colors)))
        (text-stroke-colors-hash-table
         (if (hash-table? text-stroke-colors)
             text-stroke-colors
             (alist->hash-table text-stroke-colors))))
    (%make-button textures-hash-table
                  text-fill-colors-hash-table
                  text-stroke-colors-hash-table
                  (or dimensions (max-texture-dimensions textures-hash-table)))))

;; Simplify @var{state}.
;; Return @code{pressed} if the button is pressed, @code{selected}, if it is
;; selected, or @code{none} otherwise.
(define (simplify-button-state state)
  (fold
   (match-lambda*
     (('pressed x) 'pressed)
     (('selected 'none) 'selected)
     ((_ x) x))
   'none
   state))

(define simplified-button-state (compose simplify-button-state button-state))

(define-record-type <menu>
  (make-menu accumulator validator button-callback)
  menu?
  (accumulator menu-accumulator)
  (validator menu-validator)
  (button-callback menu-button-callback))
(registry-add-component! *registry* 'menu)
