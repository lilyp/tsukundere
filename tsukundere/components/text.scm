;; Copyright © 2020, 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere components text)
  #:use-module ((baka text) #:prefix baka:)
  #:use-module (baka markup)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-71)
  #:use-module (system foreign)
  #:use-module (tsukundere components)
  #:use-module (tsukundere components textures)
  #:use-module (tsukundere game internals)
  #:use-module (tsukundere math)
  #:use-module (tsukundere preferences)
  #:export (*text-registry*
            %default-font
            text-field text-length
            reset-text! text-add!
            text-advance! text-reveal-all!
            draw-text))

(define *text-speed* (preference->procedure
                      (make-preference 'text-speed 24
                                       #:validate?
                                       (lambda (value)
                                         (and (number? value) (> value 0))))))
(define %default-font "DejaVu Sans 24")

(define *text-registry* (make-registry))

(define-component text-field *text-registry* '<baka:text-field>)
(define-component text-length *text-registry* 'length 0)
(define-component %text-revealed *text-registry* '%revealed)
(define texture (component->procedure *registry* 'texture))

;; Reset @var{text} to csontain @var{string}.  If @var{full?} is @code{#t},
;; reveal all of the text, otherwise reveal nothing.
;; If @var{markup?} is truthy, parse @var{string} as markup first and construct
;; @var{text}'s attributes accordingly.
(define* (reset-text! text string #:optional full? #:key markup?)
  (let* ((content attributes (if markup?
                                 (parse-markup string)
                                 (values string %null-pointer)))
         (length (string-length content)))
    (set! (baka:text-content (text-field text)) content)
    (unless (null-pointer? attributes)
      (set! (baka:text-attributes (text-field text)) attributes))
    (set! (text-length text) length)
    (set! (%text-revealed text) (if full? length 0))
    (set! (baka:text-num-revealed (text-field text)) (%text-revealed text))
    (baka:paint! (texture text) (text-field text))))

;; Add @var{string} to the end of @var{text}.  If @var{markup?} is truthy,
;; parse @var{string} as markup first and construct @var{text}'s attributes
;; accordingly.
(define* (text-add! text string #:key markup?)
  (if markup?
      (let ((content attributes (append-markup
                                 (baka:text-content (text-field text))
                                 (baka:text-attributes (text-field text))
                                 string)))
        (set! (baka:text-content (text-field text)) content)
        (set! (baka:text-attributes (text-field text)) attributes)
        (set! (text-length text) (string-length content)))
      (let ((existing (baka:text-content (text-field text)))
            (attributes (baka:text-attributes (text-field text)))
            (new-length (string-length string)))
        (set! (baka:text-content (text-field text))
              (string-append existing string))
        (unless (null-pointer? attributes)
          (set! (baka:text-attributes (text-field text)) attributes))
        (set! (text-length text) (+ (string-length existing) new-length)))))

;; Update the length of @var{text} according to the current text speed,
;; given that @var{delta} milliseconds have passed.
(define (text-advance! text delta)
  (let ((l (text-length text))
        (r (%text-revealed text))
        (a (* (/ delta 1000) (*text-speed*))))
    (when (< r l)
      (set! (%text-revealed text) (min l (+ r a)))
      (let ((r* (->integer (%text-revealed text))))
        (when (> r* r)
          (set! (baka:text-num-revealed (text-field text)) r*)
          (baka:paint! (texture text) (text-field text)))))))

;; Reveal all of @var{text}.  If this has lead to a change in the number of
;; revealed characters, return @code{#t}, otherwise @code{#f}.
(define (text-reveal-all! text)
  (let ((r (%text-revealed text))
        (l (text-length text)))
    (and (< r l)
         (begin
           (set! (%text-revealed text) l)
           (set! (baka:text-num-revealed (text-field text)) l)
           (baka:paint! (texture text) (text-field text))
           #t))))
