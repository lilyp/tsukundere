;; Copyright © 2020, 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere components textures)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-2)
  #:use-module (srfi srfi-26)
  #:use-module ((sdl2 blend-mode) #:prefix blend-mode:)
  #:use-module ((sdl2 image) #:prefix sdl2:)
  #:use-module (sdl2 render)
  #:use-module (sdl2 surface)
  #:use-module (tsukundere components)
  #:use-module ((tsukundere game internals) #:select (current-renderer))
  #:re-export (surface->texture)
  #:export (make-streaming-texture
            load-image
            texture-dimensions texture-width texture-height
            %render-texture))

;; FIXME: missing in Guile-SDL2 0.7.0
(define set-texture-blend-mode! (@@ (sdl2 render) set-texture-blend-mode!))

(registry-add-component! *registry* 'texture)

(define (make-streaming-texture format width height)
  (let ((texture
         (make-texture (current-renderer) format 'streaming width height)))
    (set-texture-blend-mode! texture blend-mode:blend)
    texture))

;; Return the dimensions of @var{texture} as s32vector.
(define (texture-dimensions texture)
  (call-with-values (cute query-texture texture)
    (lambda (f a w h) (s32vector w h))))

;; Return the width of @var{texture}.
(define (texture-width texture)
  (s32vector-ref (texture-dimensions texture) 0))

;; Return the height of @var{texture}.
(define (texture-height texture)
  (s32vector-ref (texture-dimensions texture) 1))

;; Load an image from @var{file} and convert it into a texture.
(define (load-image file)
  (let ((texture (call-with-surface (sdl2:load-image file)
                   (lambda (surface)
                     (surface->texture (current-renderer) surface)))))
    (set-texture-blend-mode! texture blend-mode:blend)
    texture))

(define (%render-texture texture pos anchor angle)
  (let* ((dim (texture-dimensions texture))
         (pos (anchor pos dim)))
    (render-copy (current-renderer)
                 texture
                 #:srcrect
                 (list 0 0
                       (array-ref dim 0) (array-ref dim 1))
                 #:dstrect
                 (list (array-ref pos 0) (array-ref pos 1)
                       (array-ref dim 0) (array-ref dim 1))
                 #:angle angle)))
