;; Copyright © 2021 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(defmacro : (sym)
  `(funcall (@ (guile) symbol->keyword) ',sym))

(defmacro eval-scheme (form module)
  "Evaluate FORM in MODULE."
  `(funcall (@ (guile) eval) ',form ,module))

;;; History

(defmacro history (var &optional exp)
  "Retrieve the history variable VAR.  If VAR is unbound, evaluate EXP
to determine its value."
  (if exp
      `(let* ((%mod (funcall (@ (guile) resolve-module) '(tsukundere history)))
              (%history (funcall (@ (guile) module-ref) %mod '*history*))
              (%handle (funcall (@ (guile) hashq-get-handle) %history ',var))
              (val (if %handle (cdr %handle) ,exp)))
         (unless %handle
           (funcall (@ (guile) hashq-set!) %history ',var val))
         val)
    `(funcall (@ (guile) hashq-ref)
              (funcall (@ (guile) module-ref)
                       (funcall (@ (guile) resolve-module) '(tsukundere history))
                       '*history*)
              ',var)))

(defmacro reset-history! ()
  "Clear the history."
  `(funcall (@ (guile) hash-clear!)
            (funcall (@ (guile) module-ref)
                     (funcall (@ (guile) resolve-module) '(tsukundere history))
                     '*history*)))

;;; Features

(defvar *features* '())

(defun feature? (feature)
  (memq feature *features*))

(defmacro require (feature &optional filename noerror)
  `(eval-when-compile
     (or (feature? ,feature)
         (progn
           (funcall
            (@ (system base compile) compile-and-load)
            (funcall
             (@ (guile) %search-load-path)
             (or ,filename
                 (funcall (@ (guile) string-append)
                          (funcall (@ (guile) symbol->string) ,feature)
                          ".el")))
            (: from) 'elisp)
           (or ,noerror (feature? ,feature)
               (funcall (@ (guile) error) "feature ~a was not provided"
                        ,feature))
           (feature? ,feature)))))

(defun provide (feature)
  (setq *features* (cons feature *features*)))

;;; Modules

(fset 'define-module* (@ (guile) define-module*))
(fset 'current-module (@ (guile) current-module))
(fset 'set-current-module (@ (guile) set-current-module))

(defun module-set-variable! (module symbol)
  (funcall (@ (guile) module-set!) module symbol (symbol-value symbol)))

(defun module-set-function! (module symbol)
  (funcall (@ (guile) module-set!) module symbol (symbol-function symbol)))

(defmacro import-module! (module &optional prefix)
  "Import all variables and procedures from MODULE into the global Elisp
namespace.  If given, add PREFIX as prefix to the symbol bindings."
  `(funcall (@ (guile) hash-for-each)
            (lambda (var val)
              (let* ((prefix ',prefix)
                     (var (if prefix
                              (funcall (@ (guile) symbol-append)
                                       prefix
                                       var)
                            var))
                     (val (funcall (@ (guile) variable-ref) val))
                     (proc? (funcall (@ (guile) procedure?) val))
                     (macro? (funcall (@ (guile) macro?) val)))
                (cond
                 (macro? nil)
                 (proc? (fset var val))
                 (t (set var val)))))
            (funcall (@ (guile) module-obarray)
                     (funcall (@ (guile) resolve-interface)
                              ',module))))

;;; Scripts

(fset 'gensym (@ (guile) gensym))

(defmacro forever (&rest body)
  "Run BODY forever."
  (declare (indent 0))
  (let ((agenda (gensym " agenda"))
        (current-line (gensym " current-line")))
    `(let* ((,agenda
             (funcall
              (@ (guile) eval)
              '(%script-line (current-script))
              (funcall (@ (guile) resolve-module) '(tsukundere script))))
            (,current-line
             (funcall
              (@ (guile) eval)
              '(script-line (current-script))
              (funcall (@ (guile) resolve-module) '(tsukundere script)))))
       (while t
         (funcall (@ (tsukundere agenda) reset-agenda!) ,agenda)
         (funcall (@ (tsukundere agenda) update-agenda!) ,agenda ,current-line)
         ,@body))))

(defmacro menu (var options)
  "Run a menu with OPTIONS and store the result in the history variable VAR."
  `(let ((val (funcall
               (@ (tsukundere entities menu) run-menu)
               (funcall
                (@ (tsukundere entities menu) make-menu-layer)
                ,options
                (: base)
                (funcall (@ (tsukundere entities plain) entity-at)
                         (funcall (@ (tsukundere game internals) current-scene))
                         'menu)))))
     (history ,var val)))

(defmacro with-pauses (&rest body)
  (declare (indent 0))
  "Execute BODY with a pause inserted after each statement."
  `(progn
     ,@(mapcar
        (lambda (x)
          `(prog1 ,x (funcall (@ (tsukundere) pause))))
        body)))

(provide 'tsukundere/support/base)
