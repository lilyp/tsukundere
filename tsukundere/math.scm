;; Copyright © 2020, 2021 Liliana Prikler <liliana.prikler@gmail.com>
;; Copyright © 2020, 2021 Nicolò Balzarotti <nicolo@nixo.xyz>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere math)
  #:use-module (ice-9 arrays)
  #:use-module (ice-9 match)
  #:use-module (sdl2 rect)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (degrees->radians

            smoothstep lerp
            array->rectangular make-polar* complex->vector
            same-sign?
            inside-triangle? inside-rect?

            modulo/length
            array+ array*
            ->integer array->s32vector

            anchor:top-left anchor:bottom-left anchor:center
            anchor->procedure))

;; Taken from https://oeis.org/A002485 and https://oeis.org/A002486
(define pi (/ 8958937768937 2851718461558))

;; Convert @var{degrees} to radians.
(define (degrees->radians degrees)
  (* degrees (/ pi 180)))

(define (smoothstep alpha)
  (cond
   ((<= alpha 0) 0)
   ((>= alpha 1) 1)
   (else (- (* 3 alpha alpha)
            (* 2 alpha alpha alpha)))))

(define (lerp start end alpha)
  (+ start (* (- end start) alpha)))

;; Convert @var{arr} into polar coordinates.
(define (array->rectangular arr)
  (make-rectangular (array-ref arr 0) (array-ref arr 1)))

;; Like make-polar but use @var{center} as offset.
(define* (make-polar* mag ang #:optional (center 0+0i))
  (+ (make-polar mag ang) center))

;; Return a vector holding the real and imaginary parts of @var{z}.
(define (complex->vector z)
  (vector (real-part z) (imag-part z)))

(define (complex-cross z1 z2)
  (- (* (imag-part z1) (real-part z2))
     (* (real-part z1) (imag-part z2))))

;; Return @code{#t} if @var{zs} are either all positive or all negative.
(define (same-sign? . zs)
  (or (every positive? zs)
      (every negative? zs)))

(define (inside-triangle/complex? point a b c)
  (same-sign?
   (complex-cross (- point a) (- b a))
   (complex-cross (- point b) (- c b))
   (complex-cross (- point c) (- a c))))

;; Check if @var{point} is inside the triangle spanned by @var{a}, @var{b}
;; and @var{c}.
(define (inside-triangle? point a b c)
  (inside-triangle/complex? (array->rectangular point)
                            (array->rectangular a)
                            (array->rectangular b)
                            (array->rectangular c)))

;; Check if @var{pos} is inside @var{rect} rotated around its center at
;; @var{angle}.
(define* (inside-rect? pos rect #:optional (angle 0))
  (let ((pos (array->rectangular pos))
        (center (make-rectangular (+ (rect-x rect) (/ (rect-width rect) 2))
                                  (+ (rect-y rect) (/ (rect-height rect) 2))))
        (gamma (map atan
                    (list (rect-height rect)
                          (rect-height rect)
                          (- (rect-height rect))
                          (- (rect-height rect)))
                    (list (- (rect-width rect))
                          (rect-width rect)
                          (- (rect-width rect))
                          (rect-width rect))))
        (length (/ (sqrt (+ (expt (rect-width rect) 2)
                            (expt (rect-height rect) 2)))
                   2)))

    (match (map (compose (cute make-polar* length <> center)
                         (cute + <> (degrees->radians angle)))
                gamma)
      ((a b c d)
       (or (inside-triangle/complex? pos a b c)
           (inside-triangle/complex? pos b c d))))))

(define (modulo/length idx list) (modulo idx (length list)))

;; Add @var{vectors} to a copy of @var{v} and return it.
(define (array+ v . vectors)
  (let ((out (array-copy v)))
    (apply array-map! out + v vectors)
    out))

;; Multiply @var{as} onto @var{a1} in order.
(define (array* a1 . as)
  (match as
    ((a2)
     (let* ((d1 (array-dimensions a1))
            (d2 (array-dimensions a2)))
       (match (list d1 d2)
         (((m n) (n p))
          (let ((out (make-array *unspecified* m p)))
            (array-index-map! out
                              (lambda (i j)
                                (fold (lambda (k seed)
                                        (+ seed (* (array-ref a1 i k)
                                                   (array-ref a2 k j))))
                                      0
                                      (iota n))))
            out)))))
    ((a2 . as)
     (apply array* (array* a1 a2) as))))

;; Convert @var{number} to an integer using @var{round} to round it.
(define* (->integer number #:optional (round round))
  (inexact->exact (round number)))

;; Convert @var{array}, an arbitrarily dimensioned array, to an s32vector,
;; rounding all elements to the nearest integer.
(define (array->s32vector array)
  (let ((out (make-typed-array 's32 0 (apply * (array-dimensions array)))))
    (array-map! out ->integer (array-contents array))
    out))

(define (anchor:top-left pos dim)
  pos)

(define (anchor:top-center pos dim)
  (s32vector
   (- (array-ref pos 0) (round-quotient (array-ref dim 0) 2))
   (array-ref pos 1)))

(define (anchor:center pos dim)
  (let ((out (array-copy dim)))
    (array-map! out (cute round-quotient <> 2) out)
    (array-map! out - pos out)
    out))

(define (anchor:bottom-left pos dim)
  (s32vector
   (array-ref pos 0)
   (- (array-ref pos 1) (array-ref dim 1))))

(define (anchor->procedure anchor)
  (match anchor
    ('top-left anchor:top-left)
    ('top-center anchor:top-center)
    ('bottom-left anchor:bottom-left)
    ('center anchor:center)
    ((? procedure? anchor) anchor)
    (_ (error "anchor ~a is not supported" anchor))))
