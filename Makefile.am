# Copyright © 2020, 2021 Liliana Prikler <liliana.prikler@gmail.com>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

guilesourcedir=$(datarootdir)/guile/site/$(GUILE_EFFECTIVE_VERSION)
guileobjectdir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache
guileextensiondir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/extensions

GUILEC = $(GUILE_TOOLS) compile

AM_GUILEC_FLAGS = \
  --target="$(host)" \
  -Wunbound-variable -Warity-mismatch -Wformat \
  -Wmacro-use-before-definition \
  -Wbad-case-datum -Wduplicate-case-datum

SUFFIXES = .scm .go
.scm.go:
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILEC) $(AM_GUILEC_FLAGS) $(GUILEC_FLAGS) -o "$@" "$<"

doc/snarf/%.texi: %.scm scripts/doc-snarf.go
	@mkdir -p $$(dirname "$@")
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILE_TOOLS) doc-snarf --lang=scheme --texinfo -o "$@" "$<"
	@sed -i -e "//d" "$@"

guileextension_LTLIBRARIES = libbaka.la

libbaka_la_SOURCES = \
  baka/markup.c \
  baka/text.c

libbaka_la_CFLAGS = \
  -std=c11 \
  $(GUILE_CFLAGS) \
  $(PANGOCAIRO_CFLAGS) \
  $(SDL2_CFLAGS)

libbaka_la_LDFLAGS = \
  -export-symbols-regex '^baka_' \
  -avoid-version \
  $(GUILE_LDFLAGS) \
  $(PANGOCAIRO_LDFLAGS) \
  $(SDL2_LDFLAGS)

libbaka_la_LIBADD = \
  $(GUILE_LIBS) \
  $(PANGOCAIRO_LIBS) \
  $(SDL2_LIBS)

nobase_dist_guilesource_DATA = \
  baka/markup.scm \
  baka/text.scm \
  baka/utils.scm \
  tsukundere/agenda.scm \
  tsukundere/assets.scm \
  tsukundere/cli.scm \
  tsukundere/cli/run.scm \
  tsukundere/cli/check.scm \
  tsukundere/cli/extract-dialogue.scm \
  tsukundere/cli/make-shell-script.scm \
  tsukundere/components.scm \
  tsukundere/components/menu.scm \
  tsukundere/components/textures.scm \
  tsukundere/components/text.scm \
  tsukundere/entities.scm \
  tsukundere/entities/debug.scm \
  tsukundere/entities/doll.scm \
  tsukundere/entities/menu.scm \
  tsukundere/entities/person.scm \
  tsukundere/entities/plain.scm \
  tsukundere/game.scm \
  tsukundere/game/internals.scm \
  tsukundere/game/modules.scm \
  tsukundere/game/support.scm \
  tsukundere/i18n.scm \
  tsukundere/history.scm \
  tsukundere/preferences.scm \
  tsukundere/math.scm \
  tsukundere/script.scm \
  tsukundere/script/events.scm \
  tsukundere/script/transitions.scm \
  tsukundere/script/utils.scm \
  tsukundere/sound.scm \
  tsukundere/support/base.el \
  tsukundere/support/peg.el \
  tsukundere/utils.scm \
  tsukundere.scm

info_TEXINFOS = doc/tsukundere.texi
doc_tsukundere_TEXINFOS = $(nobase_dist_guilesource_DATA:%.scm=doc/snarf/%.texi)

nobase_guileobject_DATA = $(nobase_dist_guilesource_DATA:.scm=.go)
# Ensure that shared libraries exist before trying to compile Scheme sources
$(nobase_guileobject_DATA): $(guileextension_LTLIBRARIES)

install-data-hook:
	($(MKDIR_P) ${bindir}; cd ${bindir}; $(LN_S) $(guilesourcedir)/tsukundere.scm tsukundere; chmod +x tsukundere)

uninstall-hook:
	(cd ${bindir}; rm -f tsukundere)

MOSTLYCLEANFILES = $(nobase_guileobject_DATA)
clean-local:
	-rm -rf doc/snarf scripts/doc-snarf.go

update-version: configure.ac debian/control debian/control2 guix.scm tsukundere/cli.scm
	$(SED) -i -e "s/$(OLD_VERSION)/$(NEW_VERSION)/g" $^

MAINTAINERCLEANFILES = \
  doc/tsukundere.aux \
  doc/tsukundere.cps \
  doc/tsukundere.fns \
  doc/tsukundere.log \
  doc/tsukundere.tmp \
  doc/tsukundere.tp \
  doc/tsukundere.cp \
  doc/tsukundere.fn \
  doc/tsukundere.toc \
  doc/tsukundere.tps

DISTCLEANFILES = \
  doc/tsukundere.info

EXTRA_DIST = \
  guix.scm \
  doc/fdl-1.3.texi \
  scripts/doc-snarf.scm

.SECONDARY: scripts/doc-snarf.go

ACLOCAL_AMFLAGS = -I m4
SUBDIRS = po
