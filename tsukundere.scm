#!/bin/sh
exec guile -e "(@@ (tsukundere) main)" -s "$0" "$@"
!#

;; Copyright © 2020 Liliana Prikler <liliana.prikler@gmail.com>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (tsukundere)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-37)

  #:use-module ((tsukundere assets) #:prefix assets:)
  #:use-module (tsukundere cli)
  #:use-module (tsukundere game)
  #:use-module (tsukundere history)
  #:use-module (tsukundere i18n)
  #:use-module (tsukundere preferences)
  #:use-module (tsukundere components menu)
  #:use-module (tsukundere components text)
  #:use-module (tsukundere components textures)
  #:use-module (tsukundere entities)
  #:use-module (tsukundere entities doll)
  #:use-module (tsukundere entities person)
  #:use-module (tsukundere entities plain)
  #:use-module (tsukundere entities menu)
  #:use-module (tsukundere script)
  #:use-module (tsukundere script utils)
  #:use-module (tsukundere sound)

  #:re-export ((assets:load-assets . load-assets)
               (assets:load-image . load-image)
               (assets:load-images . load-images)
               (assets:load-sound . load-sound)
               (assets:load-sounds . load-sounds)
               (assets:load-music . load-music)
               (assets:load-music* . load-music*)
               (assets:add-font-dir! . add-font-dir!)

               run-game
               call/paused
               init-menu!
               init-script!
               init-text!
               current-window
               current-renderer
               current-scene

               add-load-hook! history
               G_ N_

               make-button make-menu

               make-scene make-entity make-text
               position anchor render-function visible?
               surface->entity texture->entity texture
               layer-append! layer-cons! scene-add!
               layer-delete! scene-delete!
               layer-replace!
               layer-clear! scene-clear!
               entity-at layer-at

               load-person person->procedure
               load-doll doll->procedure

               make-preference add-preference-hook!

               script script-lambda* skripto* skripto
               forever tween
               add! clear! move!
               current-window-center background
               say make-speaker input
               menu run-menu
               roll-credits

               play-sound sound
               play-music music
               volume)
  #:re-export-and-replace (pause sleep))

(define (expand-paths args)
  (define path-name-separator?
    (case (system-file-name-convention)
      ((windows) (lambda (c) (char=? c #\;)))
      ((posix) (lambda (c) (char=? c #\:)))))

  (and=> (getenv "TSUKUNDERE_LOAD_PATH")
         (lambda (path)
           (set! %load-path
                 (append
                  (string-split path path-name-separator?)
                  %load-path))))

  (and=> (getenv "TSUKUNDERE_LOAD_COMPILED_PATH")
         (lambda (path)
           (set! %load-compiled-path
                 (append
                  (string-split path path-name-separator?)
                  %load-compiled-path))))
  args)

(define (sanitize args)
  (reverse
   (args-fold args
             (list (option '(#\h "help") #f #t
                           (lambda (opt name arg result)
                             (match (list result arg)
                               (((this) #f)
                                (cons "help" result))
                               (((this) _)
                                (cons* arg "help" result))
                               ((_ #f)
                                (cons "--help" result))
                               ((_ _)
                                (cons* arg "--help" result)))))
                   (option '(#\v "version") #f #f
                           (lambda (opt name arg result)
                             (match result
                               ((this) (cons "version" result))
                               (_ (cons "--version" result))))))
             (lambda (opt name arg result)
               (if (char? name)
                   (cons (string #\- name) result)
                   (cons (string-append "--" name) result)))
             cons
             '())))

(define %main
  (match-lambda
    ((_)
     (display (T_ "Usage: tsukundere COMMAND [OPTIONS]"))
     (newline)
     (display (T_ "Run `tsukundere help' for more information."))
     (newline)
     (exit 1))
    ((_ "help") (show-help #f))
    ((_ "help" command) (show-help command))
    ((_ "version" . _) (show-version))
    ((_ command . args) (execute command args))))

(define main (compose %main sanitize expand-paths))

;; Local Variables:
;; mode: scheme
;; End:
